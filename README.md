# What is this?

This repo contains a [Wiki](../-/wikis/home) that compiles some standard information and development practices at LAUDO. Please read it through, and feel free to make changes with a merge request.